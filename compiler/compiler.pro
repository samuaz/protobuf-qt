TEMPLATE = app
CONFIG += console c++11 c++14 c++17 c++1z
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += static

TARGET = protoc-gen-qt

INCLUDEPATH += $$PWD/third_party/include

SOURCES += main.cpp \
    protobuf_qt_generator.cpp \
    insertion_point_writer.cpp

HEADERS += \
    protobuf_qt_generator.h \
    insertion_point_writer.h \
    repeated_field_adaptor.h \
    repeated_fields.h


linux:!android {
    message("building for linux")
    LIBS += -pthread -lpthread -lstdc++ -lrt
    LIBS += -L$$PWD/third_party/lib/linux -lprotobuf -lprotoc
 }

macx: {
    message("building for macosx")
    LIBS += -L$$PWD/third_party/lib/apple -lprotobuf
}


